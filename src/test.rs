#[cfg(test)]
use crate::board;

#[test]
fn perft() {
    // startpos
    let mut board: board::Board = board::get_startpos();
    assert!(20 == board::perft(&mut board, 1));
    assert!(400 == board::perft(&mut board, 2));
    assert!(8902 == board::perft(&mut board, 3));
    assert!(197281 == board::perft(&mut board, 4));
    assert!(4865609 == board::perft(&mut board, 5));
    assert!(119060324 == board::perft(&mut board, 6));

    board = board::fen(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R",
        "w",
        "KQkq",
        "-",
        "0",
        "1",
    );
    assert!(48 == board::perft(&mut board, 1));
    assert!(2039 == board::perft(&mut board, 2));
    assert!(97862 == board::perft(&mut board, 3));
    assert!(4085603 == board::perft(&mut board, 4));
    assert!(193690690 == board::perft(&mut board, 5));
    assert!(8031647685 == board::perft(&mut board, 6));

    board = board::fen("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8", "w", "-", "-", "0", "1");
    assert!(14 == board::perft(&mut board, 1));
    assert!(191 == board::perft(&mut board, 2));
    assert!(2812 == board::perft(&mut board, 3));
    assert!(43238 == board::perft(&mut board, 4));
    assert!(674624 == board::perft(&mut board, 5));
    assert!(11030083 == board::perft(&mut board, 6));
    assert!(178633661 == board::perft(&mut board, 7));
    assert!(3009794393 == board::perft(&mut board, 8));

    board = board::fen(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1",
        "w",
        "kq",
        "-",
        "0",
        "1",
    );
    assert!(6 == board::perft(&mut board, 1));
    assert!(264 == board::perft(&mut board, 2));
    assert!(9467 == board::perft(&mut board, 3));
    assert!(422333 == board::perft(&mut board, 4));
    assert!(15833292 == board::perft(&mut board, 5));
    assert!(706045033 == board::perft(&mut board, 6));

    board = board::fen(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R",
        "b",
        "KQ",
        "-",
        "0",
        "1",
    );
    assert!(6 == board::perft(&mut board, 1));
    assert!(264 == board::perft(&mut board, 2));
    assert!(9467 == board::perft(&mut board, 3));
    assert!(422333 == board::perft(&mut board, 4));
    assert!(15833292 == board::perft(&mut board, 5));
    assert!(706045033 == board::perft(&mut board, 6));

    board = board::fen(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R",
        "w",
        "KQ",
        "-",
        "1",
        "8",
    );
    assert!(44 == board::perft(&mut board, 1));
    assert!(1486 == board::perft(&mut board, 2));
    assert!(62379 == board::perft(&mut board, 3));
    assert!(2103487 == board::perft(&mut board, 4));
    assert!(89941194 == board::perft(&mut board, 5));

    board = board::fen(
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1",
        "w",
        "-",
        "-",
        "0",
        "10",
    );
    assert!(46 == board::perft(&mut board, 1));
    assert!(2079 == board::perft(&mut board, 2));
    assert!(89890 == board::perft(&mut board, 3));
    assert!(3894594 == board::perft(&mut board, 4));
    assert!(164075551 == board::perft(&mut board, 5));
    assert!(6923051137 == board::perft(&mut board, 6));

    // impossible diagonal en passant pin
    board = board::fen("5b1k/8/8/2pP4/8/K7/8/8", "w", "-", "c6", "0", "10");
    assert!(5 == board::perft(&mut board, 1));

    board = board::fen(
        "4r1k1/p4pp1/2n2n1B/2b5/N6Q/P2q1N2/1r4PP/R4R1K",
        "b",
        "-",
        "-",
        "1",
        "23",
    );
    assert!(79 == board::perft(&mut board, 1));
    assert!(3319 == board::perft(&mut board, 2));
    assert!(233757 == board::perft(&mut board, 3));
    assert!(9913490 == board::perft(&mut board, 4));
    assert!(633315818 == board::perft(&mut board, 5));
    assert!(27076678136 == board::perft(&mut board, 6));
}
