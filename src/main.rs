mod board;
mod make_move;
mod move_gen;
mod parse_commands;
mod test;

use std::io;

fn main() {
    let mut board: board::Board = board::get_startpos();

    loop {
        let mut input: String = String::new();
        io::stdin().read_line(&mut input).unwrap();
        let commands: Vec<&str> = input.split_whitespace().collect();

        if parse_commands::start_parse(&commands, &mut board, 0, commands.len())
            == parse_commands::QUIT
        {
            break;
        }
    }
}
