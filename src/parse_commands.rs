use crate::board;

pub const QUIT: usize = 1;
pub const CONTINUE: usize = 0;

fn moves(
    commands: &[&str],
    board: &mut board::Board,
    mut current_index: usize,
    len: usize,
) -> usize {
    if current_index >= len {
        CONTINUE
    } else if commands[current_index] == "moves" {
        current_index += 1;
        while current_index < len {
            board::make_str_move(board, commands[current_index]);
            current_index += 1;
        }
        CONTINUE
    } else {
        start_parse(commands, board, current_index, len)
    }
}

fn position(
    commands: &[&str],
    board: &mut board::Board,
    current_index: usize,
    len: usize,
) -> usize {
    if current_index >= len {
        CONTINUE
    } else if commands[current_index] == "startpos" {
        *board = board::get_startpos();
        moves(commands, board, current_index + 1, len)
    } else if commands[current_index] == "fen" {
        if current_index + 7 <= len {
            *board = board::fen(
                commands[current_index + 1],
                commands[current_index + 2],
                commands[current_index + 3],
                commands[current_index + 4],
                commands[current_index + 5],
                commands[current_index + 6],
            );
            moves(commands, board, current_index + 7, len)
        } else {
            start_parse(commands, board, current_index + 1, len)
        }
    } else {
        start_parse(commands, board, current_index, len)
    }
}

fn uci(commands: &[&str], board: &mut board::Board, current_index: usize, len: usize) -> usize {
    println!("id name Nevogen Chess");
    println!("id author Caden Miller");
    println!("uciok");
    start_parse(commands, board, current_index + 1, len)
}

fn isready(commands: &[&str], board: &mut board::Board, current_index: usize, len: usize) -> usize {
    println!("readyok");
    start_parse(commands, board, current_index + 1, len)
}

fn print(commands: &[&str], board: &mut board::Board, current_index: usize, len: usize) -> usize {
    if current_index >= len {
        CONTINUE
    } else if commands[current_index] == "board" {
        board::print_board(board);
        start_parse(commands, board, current_index + 1, len)
    } else if commands[current_index] == "pad" {
        board::print_pad_map(board);
        start_parse(commands, board, current_index + 1, len)
    } else {
        start_parse(commands, board, current_index, len)
    }
}

fn go(commands: &[&str], board: &mut board::Board, current_index: usize, len: usize) -> usize {
    if current_index >= len {
        CONTINUE
    } else if commands[current_index] == "perft" {
        if current_index + 1 >= len {
            return CONTINUE;
        } else {
            let perft_depth: usize = commands[current_index + 1].parse::<usize>().unwrap_or(0);
            board::perft_start(board, perft_depth);
            return start_parse(commands, board, current_index + 2, len);
        }
    } else {
        start_parse(commands, board, current_index, len)
    }
}

pub fn start_parse(
    commands: &[&str],
    board: &mut board::Board,
    current_index: usize,
    len: usize,
) -> usize {
    if current_index >= len {
        CONTINUE
    } else if commands[current_index] == "quit" {
        QUIT
    } else if commands[current_index] == "position" {
        position(commands, board, current_index + 1, len)
    } else if commands[current_index] == "uci" {
        uci(commands, board, current_index, len)
    } else if commands[current_index] == "isready" {
        isready(commands, board, current_index, len)
    } else if commands[current_index] == "print" {
        print(commands, board, current_index + 1, len)
    } else if commands[current_index] == "go" {
        go(commands, board, current_index + 1, len)
    } else {
        start_parse(commands, board, current_index + 1, len)
    }
}
