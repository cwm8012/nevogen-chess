use crate::board;

/*
no check -> not a pin
no check -> a pin -> pin attack defend = pin type
check -> not a pin -> not castle -> attack defend -> ep attack defend pawn move
double check -> king move -> not castle

some of these are accounted for in the move generator
 */
fn if_legal_add(board: &board::Board, bmove_list: &mut board::BmoveList) {
    let from_sindex: usize = bmove_list.list[bmove_list.index].from_sindex as usize;
    let to_sindex: usize = bmove_list.list[bmove_list.index].to_sindex as usize;
    let from_ptype: usize = bmove_list.list[bmove_list.index].from_ptype as usize;

    let check_count: usize = board.check_count;
    let pad_from: usize = board.pad_map[from_sindex];
    let pad_to: usize = board.pad_map[to_sindex];

    if check_count == 0 && (pad_from == board::PAD_UNSET || pad_from == pad_to)
        || check_count == 1
            && (pad_to == board::PAD_CHECK_AD
                || from_ptype == board::PAWN && pad_to == board::PAD_EP_CHECK_AD)
    {
        bmove_list.index += 1;
    } else {
        bmove_list.list[bmove_list.index] = board::EMPTY_BMOVE;
    }
}

// for checking if a sqr is attacked when not moving the king
fn is_sqr_attacked(board: &board::Board, sindex: &usize) -> bool {
    // check diagonol slider attacks
    for offset in board::BISHOP_OFFSETS.iter() {
        let mut curr_sindex: usize = *sindex;

        loop {
            curr_sindex = (curr_sindex as isize + offset) as usize;

            if board.sqrs[curr_sindex].stype == board::EMPTY {
                continue;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::BISHOP
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                return true;
            } else {
                break;
            }
        }
    }

    // check horizontal vertical slider attacks
    for offset in board::ROOK_OFFSETS.iter() {
        let mut curr_sindex: usize = *sindex;

        loop {
            curr_sindex = (curr_sindex as isize + offset) as usize;

            if board.sqrs[curr_sindex].stype == board::EMPTY {
                continue;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::ROOK
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                return true;
            } else {
                break;
            }
        }
    }

    // check pawn attacks
    let mut pawn_attacks: [isize; 2] = board::WPAWN_OFFSETS;
    if board.stm == board::BLACK {
        pawn_attacks = board::BPAWN_OFFSETS;
    }
    for offset in pawn_attacks.iter() {
        let attack_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[attack_sindex].stype == board.non_stm
            && board.sqrs[attack_sindex].ptype == board::PAWN
        {
            return true;
        }
    }

    // check knight attacks
    for offset in board::KNIGHT_OFFSETS.iter() {
        let curr_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::KNIGHT
        {
            return true;
        }
    }

    // check king attacks
    for offset in board::KING_QUEEN_OFFSETS.iter() {
        let curr_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::KING
        {
            return true;
        }
    }

    false
}

// for checking if a sqr is attacked when moving the king
fn is_king_sqr_attacked(board: &board::Board, sindex: &usize) -> bool {
    // check diagonol slider attacks
    for offset in board::BISHOP_OFFSETS.iter() {
        let mut curr_sindex: usize = *sindex;

        loop {
            curr_sindex = (curr_sindex as isize + offset) as usize;

            if board.sqrs[curr_sindex].stype == board::EMPTY
                || (board.sqrs[curr_sindex].stype == board.stm
                    && board.sqrs[curr_sindex].ptype == board::KING)
            {
                continue;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::BISHOP
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                return true;
            } else {
                break;
            }
        }
    }

    // check horizontal vertical slider attacks
    for offset in board::ROOK_OFFSETS.iter() {
        let mut curr_sindex: usize = *sindex;

        loop {
            curr_sindex = (curr_sindex as isize + offset) as usize;

            if board.sqrs[curr_sindex].stype == board::EMPTY
                || (board.sqrs[curr_sindex].stype == board.stm
                    && board.sqrs[curr_sindex].ptype == board::KING)
            {
                continue;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::ROOK
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                return true;
            } else {
                break;
            }
        }
    }

    // check pawn attacks
    let mut pawn_attacks: [isize; 2] = board::WPAWN_OFFSETS;
    if board.stm == board::BLACK {
        pawn_attacks = board::BPAWN_OFFSETS;
    }
    for offset in pawn_attacks.iter() {
        let attack_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[attack_sindex].stype == board.non_stm
            && board.sqrs[attack_sindex].ptype == board::PAWN
        {
            return true;
        }
    }

    // check knight attacks
    for offset in board::KNIGHT_OFFSETS.iter() {
        let curr_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::KNIGHT
        {
            return true;
        }
    }

    // check king attacks
    for offset in board::KING_QUEEN_OFFSETS.iter() {
        let curr_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::KING
        {
            return true;
        }
    }

    false
}

fn update_pad_map(board: &mut board::Board) {
    board.check_count = 0;
    board.pad_map = [board::PAD_UNSET; board::SQRS_LEN];
    board.ep_pin = board::PAD_UNSET;

    let king_sindex: usize = board.plist[board.stm];

    // diagonal sliders
    for offset in board::BISHOP_OFFSETS.iter() {
        let mut curr_sindex: usize = (king_sindex as isize + offset) as usize;
        let mut pins: usize = 0;
        let mut ep_pins: usize = 0;

        while board.sqrs[curr_sindex].stype != board::OFFBOARD {
            if curr_sindex == board.ep_cap_sqr && pins == 0 {
                pins += 1;
                ep_pins += 1;
            } else if board.sqrs[curr_sindex].stype == board.stm && pins == 0 {
                pins += 1;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::BISHOP
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                let mut pad_settings = board::PAD_CHECK_AD;
                if ep_pins != 0 {
                    board.ep_pin = board::PAD_EP_CHECK_AD;
                    break;
                } else if pins != 0 {
                    pad_settings = curr_sindex;
                } else {
                    board.check_count += 1;
                }

                while curr_sindex != king_sindex {
                    board.pad_map[curr_sindex] = pad_settings;
                    curr_sindex = (curr_sindex as isize - offset) as usize;
                }
                break;
            } else if board.sqrs[curr_sindex].stype != board::EMPTY {
                break;
            }
            curr_sindex = (curr_sindex as isize + offset) as usize;
        }
    }

    // horizontal vertical sliders
    for offset in board::ROOK_OFFSETS.iter() {
        let mut curr_sindex: usize = (king_sindex as isize + offset) as usize;
        let mut next_sindex: usize = (curr_sindex as isize + offset) as usize;
        let mut pins: usize = 0;
        let mut ep_pins: usize = 0;

        while board.sqrs[curr_sindex].stype != board::OFFBOARD {
            if (*offset == board::LEFT_DIR || *offset == board::RIGHT_DIR)
                && board.sqrs[curr_sindex].ptype == board::PAWN
                && board.sqrs[next_sindex].ptype == board::PAWN
                && (curr_sindex == board.ep_cap_sqr || next_sindex == board.ep_cap_sqr)
                && (board.sqrs[curr_sindex].stype == board.stm
                    || board.sqrs[next_sindex].stype == board.stm)
                && pins == 0
            {
                pins += 1;
                ep_pins += 1;
                curr_sindex = (curr_sindex as isize + offset) as usize;
                next_sindex = (next_sindex as isize + offset) as usize;
            } else if board.sqrs[curr_sindex].stype == board.stm && pins == 0 {
                pins += 1;
            } else if board.sqrs[curr_sindex].stype == board.non_stm
                && (board.sqrs[curr_sindex].ptype == board::ROOK
                    || board.sqrs[curr_sindex].ptype == board::QUEEN)
            {
                let mut pad_settings = board::PAD_CHECK_AD;
                if ep_pins != 0 {
                    board.ep_pin = board::PAD_EP_CHECK_AD;
                    break;
                } else if pins != 0 {
                    pad_settings = curr_sindex;
                } else {
                    board.check_count += 1;
                }

                while curr_sindex != king_sindex {
                    board.pad_map[curr_sindex] = pad_settings;
                    curr_sindex = (curr_sindex as isize - offset) as usize;
                }
                break;
            } else if board.sqrs[curr_sindex].stype != board::EMPTY {
                break;
            }
            curr_sindex = (curr_sindex as isize + offset) as usize;
            next_sindex = (next_sindex as isize + offset) as usize;
        }
    }

    // check count, pad maps for pawns
    let mut pawn_offsets: [isize; 2] = board::WPAWN_OFFSETS;
    if board.stm == board::BLACK {
        pawn_offsets = board::BPAWN_OFFSETS;
    }
    for offset in pawn_offsets.iter() {
        let curr_sindex: usize = (king_sindex as isize + offset) as usize;
        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::PAWN
        {
            board.check_count += 1;
            board.pad_map[curr_sindex] = board::PAD_CHECK_AD;

            if curr_sindex == board.ep_cap_sqr {
                board.pad_map[board.ep_sqr] = board::PAD_EP_CHECK_AD;
            }
        }
    }

    // check count, pad maps for knights
    for offset in board::KNIGHT_OFFSETS.iter() {
        let curr_sindex: usize = (king_sindex as isize + offset) as usize;
        if board.sqrs[curr_sindex].stype == board.non_stm
            && board.sqrs[curr_sindex].ptype == board::KNIGHT
        {
            board.check_count += 1;
            board.pad_map[curr_sindex] = board::PAD_CHECK_AD;
        }
    }
}

fn pawn_move_gen(board: &board::Board, bmove_list: &mut board::BmoveList, sindex: &usize) {
    if board.check_count == 2
        || board.check_count == 1 && board.pad_map[*sindex] != board::PAD_UNSET
    {
        return;
    }

    let mut pawn_forward: usize = (*sindex as isize + board::WPAWN_FORWARD) as usize;
    let mut pawn_forward_two: usize = (*sindex as isize + 2 * board::WPAWN_FORWARD) as usize;
    let mut is_prank: bool = (board::A8..=board::H8).contains(&pawn_forward);
    let mut is_srank: bool = (board::A2..=board::H2).contains(sindex);
    let mut pawn_attacks: [isize; 2] = board::WPAWN_OFFSETS;

    if board.stm == board::BLACK {
        pawn_forward = (*sindex as isize + board::BPAWN_FORWARD) as usize;
        pawn_forward_two = (*sindex as isize + 2 * board::BPAWN_FORWARD) as usize;
        is_prank = (board::A1..=board::H1).contains(&pawn_forward);
        is_srank = (board::A7..=board::H7).contains(sindex);
        pawn_attacks = board::BPAWN_OFFSETS;
    }

    // non attacking moves
    if !is_prank && board.sqrs[pawn_forward].stype == board::EMPTY {
        bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
        bmove_list.list[bmove_list.index].to_sindex = pawn_forward as u8;
        bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
        if_legal_add(board, bmove_list);

        if is_srank && board.sqrs[pawn_forward_two].stype == board::EMPTY {
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = pawn_forward_two as u8;
            bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
            bmove_list.list[bmove_list.index].ep_sindex = pawn_forward as u8;
            bmove_list.list[bmove_list.index].ep_cap_sindex = pawn_forward_two as u8;
            if_legal_add(board, bmove_list);
        }
    } else if is_prank && board.sqrs[pawn_forward].stype == board::EMPTY {
        for promotion_ptype in board::KNIGHT..=board::QUEEN {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_PROMOTION;
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = pawn_forward as u8;
            bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
            bmove_list.list[bmove_list.index].promotion_ptype = promotion_ptype as u8;
            if_legal_add(board, bmove_list);
        }
    }

    // attacking moves
    for offset in pawn_attacks.iter() {
        let attack_sindex: usize = (*sindex as isize + offset) as usize;

        if is_prank && board.sqrs[attack_sindex].stype == board.non_stm {
            for promotion_ptype in board::KNIGHT..=board::QUEEN {
                bmove_list.list[bmove_list.index].mtype = board::BMOVE_PROMOTION | board::BMOVE_CAP;
                bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
                bmove_list.list[bmove_list.index].to_sindex = attack_sindex as u8;
                bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
                bmove_list.list[bmove_list.index].to_ptype = board.sqrs[attack_sindex].ptype as u8;
                bmove_list.list[bmove_list.index].promotion_ptype = promotion_ptype as u8;
                if_legal_add(board, bmove_list);
            }
        } else if board.sqrs[attack_sindex].stype == board.non_stm {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CAP;
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = attack_sindex as u8;
            bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
            bmove_list.list[bmove_list.index].to_ptype = board.sqrs[attack_sindex].ptype as u8;
            if_legal_add(board, bmove_list);
        } else if attack_sindex == board.ep_sqr && board.ep_pin == board::PAD_UNSET {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_EP;
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = attack_sindex as u8;
            bmove_list.list[bmove_list.index].from_ptype = board::PAWN as u8;
            bmove_list.list[bmove_list.index].to_ptype = board::PAWN as u8;
            if_legal_add(board, bmove_list);
        }
    }
}

fn knight_move_gen(board: &board::Board, bmove_list: &mut board::BmoveList, sindex: &usize) {
    if board.check_count == 2
        || board.check_count == 1 && board.pad_map[*sindex] != board::PAD_UNSET
    {
        return;
    }

    for offset in board::KNIGHT_OFFSETS.iter() {
        let to_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[to_sindex].stype == board::EMPTY {
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = to_sindex as u8;
            if_legal_add(board, bmove_list);
        } else if board.sqrs[to_sindex].stype == board.non_stm {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CAP;
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = to_sindex as u8;
            bmove_list.list[bmove_list.index].to_ptype = board.sqrs[to_sindex].ptype as u8;
            if_legal_add(board, bmove_list);
        }
    }
}

fn king_move_gen(board: &board::Board, bmove_list: &mut board::BmoveList, sindex: &usize) {
    for offset in board::KING_QUEEN_OFFSETS.iter() {
        let curr_sindex: usize = (*sindex as isize + offset) as usize;

        if board.sqrs[curr_sindex].stype == board::EMPTY
            && !is_king_sqr_attacked(board, &curr_sindex)
        {
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = curr_sindex as u8;
            bmove_list.index += 1;
        } else if board.sqrs[curr_sindex].stype == board.non_stm
            && !is_king_sqr_attacked(board, &curr_sindex)
        {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CAP;
            bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
            bmove_list.list[bmove_list.index].to_sindex = curr_sindex as u8;
            bmove_list.list[bmove_list.index].to_ptype = board.sqrs[curr_sindex].ptype as u8;
            bmove_list.index += 1;
        }
    }

    if board.check_count != 0 {
        return;
    }
    if board.stm == board::WHITE {
        if board.castle & board::CASTLE_WK == board::CASTLE_WK
            && board.sqrs[board::F1].stype == board::EMPTY
            && board.sqrs[board::G1].stype == board::EMPTY
            && !is_sqr_attacked(board, &board::F1)
            && !is_sqr_attacked(board, &board::G1)
        {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CASTLE;
            bmove_list.list[bmove_list.index].from_sindex = board::E1 as u8;
            bmove_list.list[bmove_list.index].to_sindex = board::G1 as u8;
            bmove_list.index += 1;
        }
        if board.castle & board::CASTLE_WQ == board::CASTLE_WQ
            && board.sqrs[board::D1].stype == board::EMPTY
            && board.sqrs[board::C1].stype == board::EMPTY
            && board.sqrs[board::B1].stype == board::EMPTY
            && !is_sqr_attacked(board, &board::D1)
            && !is_sqr_attacked(board, &board::C1)
        {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CASTLE;
            bmove_list.list[bmove_list.index].from_sindex = board::E1 as u8;
            bmove_list.list[bmove_list.index].to_sindex = board::C1 as u8;
            bmove_list.index += 1;
        }
    } else if board.stm == board::BLACK {
        if board.castle & board::CASTLE_BK == board::CASTLE_BK
            && board.sqrs[board::F8].stype == board::EMPTY
            && board.sqrs[board::G8].stype == board::EMPTY
            && !is_sqr_attacked(board, &board::F8)
            && !is_sqr_attacked(board, &board::G8)
        {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CASTLE;
            bmove_list.list[bmove_list.index].from_sindex = board::E8 as u8;
            bmove_list.list[bmove_list.index].to_sindex = board::G8 as u8;
            bmove_list.index += 1;
        }
        if board.castle & board::CASTLE_BQ == board::CASTLE_BQ
            && board.sqrs[board::D8].stype == board::EMPTY
            && board.sqrs[board::C8].stype == board::EMPTY
            && board.sqrs[board::B8].stype == board::EMPTY
            && !is_sqr_attacked(board, &board::D8)
            && !is_sqr_attacked(board, &board::C8)
        {
            bmove_list.list[bmove_list.index].mtype = board::BMOVE_CASTLE;
            bmove_list.list[bmove_list.index].from_sindex = board::E8 as u8;
            bmove_list.list[bmove_list.index].to_sindex = board::C8 as u8;
            bmove_list.index += 1;
        }
    }
}

fn slider_move_gen(
    board: &board::Board,
    bmove_list: &mut board::BmoveList,
    sindex: &usize,
    offsets: &[isize],
) {
    if board.check_count == 2
        || board.check_count == 1 && board.pad_map[*sindex] != board::PAD_UNSET
    {
        return;
    }

    for offset in offsets.iter() {
        let mut curr_sindex: usize = *sindex;

        loop {
            curr_sindex = (curr_sindex as isize + offset) as usize;

            if board.sqrs[curr_sindex].stype == board::EMPTY {
                bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
                bmove_list.list[bmove_list.index].to_sindex = curr_sindex as u8;
                if_legal_add(board, bmove_list);
            } else if board.sqrs[curr_sindex].stype == board.non_stm {
                bmove_list.list[bmove_list.index].mtype = board::BMOVE_CAP;
                bmove_list.list[bmove_list.index].from_sindex = *sindex as u8;
                bmove_list.list[bmove_list.index].to_sindex = curr_sindex as u8;
                bmove_list.list[bmove_list.index].to_ptype = board.sqrs[curr_sindex].ptype as u8;
                if_legal_add(board, bmove_list);
                break;
            } else {
                break;
            }
        }
    }
}

pub fn move_gen(board: &mut board::Board, bmove_list: &mut board::BmoveList) {
    update_pad_map(board);

    for sindex in board.plist[board.stm..board.plist_len[board.stm]].iter() {
        if board.sqrs[*sindex].ptype == board::PAWN {
            pawn_move_gen(board, bmove_list, sindex);
        } else if board.sqrs[*sindex].ptype == board::KNIGHT {
            knight_move_gen(board, bmove_list, sindex);
        } else if board.sqrs[*sindex].ptype == board::BISHOP {
            slider_move_gen(board, bmove_list, sindex, &board::BISHOP_OFFSETS);
        } else if board.sqrs[*sindex].ptype == board::ROOK {
            slider_move_gen(board, bmove_list, sindex, &board::ROOK_OFFSETS);
        } else if board.sqrs[*sindex].ptype == board::QUEEN {
            slider_move_gen(board, bmove_list, sindex, &board::KING_QUEEN_OFFSETS);
        } else if board.sqrs[*sindex].ptype == board::KING {
            king_move_gen(board, bmove_list, sindex);
        }
    }

    // update game state
    if bmove_list.index == 0 {
        // stalemate or non stm won by checkmate
        if board.check_count == 0 {
            board.game_state = board::GS_DRAW;
        } else {
            board.game_state = board.non_stm;
        }
    }

    // halfmove draw
    if board.halfmove == 100 {
        board.game_state = board::GS_DRAW;
        bmove_list.index = 0;
    }
}
