use crate::board;

pub fn make_move(board: &mut board::Board, bmove: &board::Bmove) {
    // capture, ep, castle
    if bmove.mtype & board::BMOVE_CAP == board::BMOVE_CAP {
        board::remove_piece(board, &(bmove.to_sindex as usize));
    } else if bmove.mtype & board::BMOVE_EP == board::BMOVE_EP {
        let ep_cap_sqr: usize = board.ep_cap_sqr;
        board::remove_piece(board, &ep_cap_sqr);
    } else if bmove.mtype & board::BMOVE_CASTLE == board::BMOVE_CASTLE {
        if bmove.to_sindex as usize == board::G1 {
            board.sqrs[board::F1].stype = board.sqrs[board::H1].stype;
            board.sqrs[board::F1].ptype = board.sqrs[board::H1].ptype;
            board.sqrs[board::F1].pindex = board.sqrs[board::H1].pindex;
            board.sqrs[board::H1] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::F1].pindex] = board::F1;
        } else if bmove.to_sindex as usize == board::C1 {
            board.sqrs[board::D1].stype = board.sqrs[board::A1].stype;
            board.sqrs[board::D1].ptype = board.sqrs[board::A1].ptype;
            board.sqrs[board::D1].pindex = board.sqrs[board::A1].pindex;
            board.sqrs[board::A1] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::D1].pindex] = board::D1;
        } else if bmove.to_sindex as usize == board::G8 {
            board.sqrs[board::F8].stype = board.sqrs[board::H8].stype;
            board.sqrs[board::F8].ptype = board.sqrs[board::H8].ptype;
            board.sqrs[board::F8].pindex = board.sqrs[board::H8].pindex;
            board.sqrs[board::H8] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::F8].pindex] = board::F8;
        } else if bmove.to_sindex as usize == board::C8 {
            board.sqrs[board::D8].stype = board.sqrs[board::A8].stype;
            board.sqrs[board::D8].ptype = board.sqrs[board::A8].ptype;
            board.sqrs[board::D8].pindex = board.sqrs[board::A8].pindex;
            board.sqrs[board::A8] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::D8].pindex] = board::D8;
        }
    }

    // promotion
    if bmove.mtype & board::BMOVE_PROMOTION == board::BMOVE_PROMOTION {
        board.sqrs[bmove.from_sindex as usize].ptype = bmove.promotion_ptype as usize;
    }

    // move piece
    board.sqrs[bmove.to_sindex as usize].stype = board.sqrs[bmove.from_sindex as usize].stype;
    board.sqrs[bmove.to_sindex as usize].ptype = board.sqrs[bmove.from_sindex as usize].ptype;
    board.sqrs[bmove.to_sindex as usize].pindex = board.sqrs[bmove.from_sindex as usize].pindex;
    board.sqrs[bmove.from_sindex as usize] = board::EMPTY_SQUARE;
    board.plist[board.sqrs[bmove.to_sindex as usize].pindex] = bmove.to_sindex as usize;

    // inc history
    board.castle_history[board.history_index] = board.castle;
    board.ep_sqr_history[board.history_index] = board.ep_sqr;
    board.ep_cap_sqr_history[board.history_index] = board.ep_cap_sqr;
    board.halfmove_history[board.history_index] = board.halfmove;
    board.history_index += 1;

    // white king side castle check
    if board.castle & board::CASTLE_WK == board::CASTLE_WK
        && (board.sqrs[board::E1].stype != board::WHITE
            || board.sqrs[board::H1].stype != board::WHITE)
    {
        board.castle ^= board::CASTLE_WK;
    }
    // white queen side castle check
    if board.castle & board::CASTLE_WQ == board::CASTLE_WQ
        && (board.sqrs[board::E1].stype != board::WHITE
            || board.sqrs[board::A1].stype != board::WHITE)
    {
        board.castle ^= board::CASTLE_WQ;
    }
    // black king side castle check
    if board.castle & board::CASTLE_BK == board::CASTLE_BK
        && (board.sqrs[board::E8].stype != board::BLACK
            || board.sqrs[board::H8].stype != board::BLACK)
    {
        board.castle ^= board::CASTLE_BK;
    }
    // black queen side castle check
    if board.castle & board::CASTLE_BQ == board::CASTLE_BQ
        && (board.sqrs[board::E8].stype != board::BLACK
            || board.sqrs[board::A8].stype != board::BLACK)
    {
        board.castle ^= board::CASTLE_BQ;
    }

    // update ep sqr and ep cap sqr
    board.ep_sqr = bmove.ep_sindex as usize;
    board.ep_cap_sqr = bmove.ep_cap_sindex as usize;

    // update halfmove
    if bmove.from_ptype as usize == board::PAWN
        || bmove.mtype & board::BMOVE_CAP == board::BMOVE_CAP
    {
        board.halfmove = 0;
    } else {
        board.halfmove += 1;
    }

    // update fullmove
    if board.stm == board::BLACK {
        board.fullmove += 1;
    }

    // update stm & non stm
    if board.stm == board::WHITE {
        board.stm = board::BLACK;
        board.non_stm = board::WHITE;
    } else if board.stm == board::BLACK {
        board.stm = board::WHITE;
        board.non_stm = board::BLACK;
    }
}

pub fn unmake_move(board: &mut board::Board, bmove: &board::Bmove) {
    // update stm & non stm
    if board.stm == board::WHITE {
        board.stm = board::BLACK;
        board.non_stm = board::WHITE;
    } else if board.stm == board::BLACK {
        board.stm = board::WHITE;
        board.non_stm = board::BLACK;
    }

    // update fullmove
    if board.stm == board::BLACK {
        board.fullmove -= 1;
    }

    // dec history
    board.history_index -= 1;
    board.castle = board.castle_history[board.history_index];
    board.ep_sqr = board.ep_sqr_history[board.history_index];
    board.ep_cap_sqr = board.ep_cap_sqr_history[board.history_index];
    board.halfmove = board.halfmove_history[board.history_index];

    // unmove piece
    board.sqrs[bmove.from_sindex as usize].stype = board.sqrs[bmove.to_sindex as usize].stype;
    board.sqrs[bmove.from_sindex as usize].ptype = board.sqrs[bmove.to_sindex as usize].ptype;
    board.sqrs[bmove.from_sindex as usize].pindex = board.sqrs[bmove.to_sindex as usize].pindex;
    board.sqrs[bmove.to_sindex as usize] = board::EMPTY_SQUARE;
    board.plist[board.sqrs[bmove.from_sindex as usize].pindex] = bmove.from_sindex as usize;

    // promotion
    if bmove.mtype & board::BMOVE_PROMOTION == board::BMOVE_PROMOTION {
        board.sqrs[bmove.from_sindex as usize].ptype = bmove.from_ptype as usize;
    }

    // capture, ep, castle
    if bmove.mtype & board::BMOVE_CAP == board::BMOVE_CAP {
        let color: usize = board.non_stm;
        board::add_piece(
            board,
            &color,
            &(bmove.to_ptype as usize),
            &(bmove.to_sindex as usize),
        );
    } else if bmove.mtype & board::BMOVE_EP == board::BMOVE_EP {
        let color: usize = board.non_stm;
        let ep_cap_sqr: usize = board.ep_cap_sqr;
        board::add_piece(board, &color, &board::PAWN, &ep_cap_sqr);
    } else if bmove.mtype & board::BMOVE_CASTLE == board::BMOVE_CASTLE {
        if bmove.to_sindex as usize == board::G1 {
            board.sqrs[board::H1].stype = board.sqrs[board::F1].stype;
            board.sqrs[board::H1].ptype = board.sqrs[board::F1].ptype;
            board.sqrs[board::H1].pindex = board.sqrs[board::F1].pindex;
            board.sqrs[board::F1] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::H1].pindex] = board::H1;
        } else if bmove.to_sindex as usize == board::C1 {
            board.sqrs[board::A1].stype = board.sqrs[board::D1].stype;
            board.sqrs[board::A1].ptype = board.sqrs[board::D1].ptype;
            board.sqrs[board::A1].pindex = board.sqrs[board::D1].pindex;
            board.sqrs[board::D1] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::A1].pindex] = board::A1;
        } else if bmove.to_sindex as usize == board::G8 {
            board.sqrs[board::H8].stype = board.sqrs[board::F8].stype;
            board.sqrs[board::H8].ptype = board.sqrs[board::F8].ptype;
            board.sqrs[board::H8].pindex = board.sqrs[board::F8].pindex;
            board.sqrs[board::F8] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::H8].pindex] = board::H8;
        } else if bmove.to_sindex as usize == board::C8 {
            board.sqrs[board::A8].stype = board.sqrs[board::D8].stype;
            board.sqrs[board::A8].ptype = board.sqrs[board::D8].ptype;
            board.sqrs[board::A8].pindex = board.sqrs[board::D8].pindex;
            board.sqrs[board::D8] = board::EMPTY_SQUARE;
            board.plist[board.sqrs[board::A8].pindex] = board::A8;
        }
    }
}
