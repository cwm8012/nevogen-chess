use crate::make_move;
use crate::move_gen;
use std::time::Instant;

// lens
pub const SQRS_LEN: usize = 192;
const SQRS_WIDTH: usize = 16;
const PLIST_LEN: usize = 131;
const MAILBOX_LEN: usize = 64;
const MAILBOX_HEIGHT_WIDTH: usize = 8;
const BMOVE_LIST_LEN: usize = 250;
const HISTORY_LEN: usize = 250;

// square types
pub const EMPTY: usize = 0;
pub const OFFBOARD: usize = 1;
pub const NO_EP: usize = 2;
pub const WHITE: usize = 3;
pub const BLACK: usize = 67;

// piece types
pub const NO_PIECE: usize = 0;
pub const PAWN: usize = 1;
pub const KNIGHT: usize = 2;
pub const BISHOP: usize = 3;
pub const ROOK: usize = 4;
pub const QUEEN: usize = 5;
pub const KING: usize = 6;

// castle rights
pub const NO_CASTLE: usize = 0;
pub const CASTLE_WK: usize = 1;
pub const CASTLE_WQ: usize = 2;
pub const CASTLE_BK: usize = 4;
pub const CASTLE_BQ: usize = 8;

// square locations
pub const A8: usize = 36;
pub const B8: usize = 37;
pub const C8: usize = 38;
pub const D8: usize = 39;
pub const E8: usize = 40;
pub const F8: usize = 41;
pub const G8: usize = 42;
pub const H8: usize = 43;

pub const A7: usize = 52;
pub const B7: usize = 53;
pub const C7: usize = 54;
pub const D7: usize = 55;
pub const E7: usize = 56;
pub const F7: usize = 57;
pub const G7: usize = 58;
pub const H7: usize = 59;

pub const A6: usize = 68;
pub const B6: usize = 69;
pub const C6: usize = 70;
pub const D6: usize = 71;
pub const E6: usize = 72;
pub const F6: usize = 73;
pub const G6: usize = 74;
pub const H6: usize = 75;

pub const A5: usize = 84;
pub const B5: usize = 85;
pub const C5: usize = 86;
pub const D5: usize = 87;
pub const E5: usize = 88;
pub const F5: usize = 89;
pub const G5: usize = 90;
pub const H5: usize = 91;

pub const A4: usize = 100;
pub const B4: usize = 101;
pub const C4: usize = 102;
pub const D4: usize = 103;
pub const E4: usize = 104;
pub const F4: usize = 105;
pub const G4: usize = 106;
pub const H4: usize = 107;

pub const A3: usize = 116;
pub const B3: usize = 117;
pub const C3: usize = 118;
pub const D3: usize = 119;
pub const E3: usize = 120;
pub const F3: usize = 121;
pub const G3: usize = 122;
pub const H3: usize = 123;

pub const A2: usize = 132;
pub const B2: usize = 133;
pub const C2: usize = 134;
pub const D2: usize = 135;
pub const E2: usize = 136;
pub const F2: usize = 137;
pub const G2: usize = 138;
pub const H2: usize = 139;

pub const A1: usize = 148;
pub const B1: usize = 149;
pub const C1: usize = 150;
pub const D1: usize = 151;
pub const E1: usize = 152;
pub const F1: usize = 153;
pub const G1: usize = 154;
pub const H1: usize = 155;

pub const MAILBOX: [usize; MAILBOX_LEN] = [
    A8, B8, C8, D8, E8, F8, G8, H8, A7, B7, C7, D7, E7, F7, G7, H7, A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5, A4, B4, C4, D4, E4, F4, G4, H4, A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2, A1, B1, C1, D1, E1, F1, G1, H1,
];

const SQUARE_NAMES: [&str; SQRS_LEN] = [
    "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",
    "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "a8",
    "b8", "c8", "d8", "e8", "f8", "g8", "h8", "-", "-", "-", "-", "-", "-", "-", "-", "a7", "b7",
    "c7", "d7", "e7", "f7", "g7", "h7", "-", "-", "-", "-", "-", "-", "-", "-", "a6", "b6", "c6",
    "d6", "e6", "f6", "g6", "h6", "-", "-", "-", "-", "-", "-", "-", "-", "a5", "b5", "c5", "d5",
    "e5", "f5", "g5", "h5", "-", "-", "-", "-", "-", "-", "-", "-", "a4", "b4", "c4", "d4", "e4",
    "f4", "g4", "h4", "-", "-", "-", "-", "-", "-", "-", "-", "a3", "b3", "c3", "d3", "e3", "f3",
    "g3", "h3", "-", "-", "-", "-", "-", "-", "-", "-", "a2", "b2", "c2", "d2", "e2", "f2", "g2",
    "h2", "-", "-", "-", "-", "-", "-", "-", "-", "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",
    "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",
    "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",
];

// pin, attack defend map settings
pub const PAD_UNSET: usize = 0;
pub const PAD_CHECK_AD: usize = 1;
pub const PAD_EP_CHECK_AD: usize = 2;

// piece offsets
pub const LEFT_DIR: isize = -1;
pub const RIGHT_DIR: isize = 1;
pub const WPAWN_FORWARD: isize = -16;
pub const BPAWN_FORWARD: isize = 16;
pub const WPAWN_OFFSETS: [isize; 2] = [-17, -15];
pub const BPAWN_OFFSETS: [isize; 2] = [17, 15];
pub const KNIGHT_OFFSETS: [isize; 8] = [-18, -33, -31, -14, 18, 33, 31, 14];
pub const BISHOP_OFFSETS: [isize; 4] = [-17, -15, 17, 15];
pub const ROOK_OFFSETS: [isize; 4] = [-1, -16, 1, 16];
pub const KING_QUEEN_OFFSETS: [isize; 8] = [-17, -16, -15, -1, 1, 15, 16, 17];

// game state
// if won, game state equals the stm that won
pub const GS_ONGOING: usize = EMPTY;
pub const GS_DRAW: usize = OFFBOARD;

const OFFBOARD_SQUARE: Square = Square {
    stype: OFFBOARD,
    ptype: NO_PIECE,
    pindex: EMPTY,
};

pub const EMPTY_SQUARE: Square = Square {
    stype: EMPTY,
    ptype: NO_PIECE,
    pindex: EMPTY,
};

pub const BMOVE_NORMAL: u8 = 0;
pub const BMOVE_CAP: u8 = 1;
pub const BMOVE_PROMOTION: u8 = 2;
pub const BMOVE_EP: u8 = 4;
pub const BMOVE_CASTLE: u8 = 8;

pub const EMPTY_BMOVE: Bmove = Bmove {
    mtype: BMOVE_NORMAL,
    from_sindex: EMPTY as u8,
    to_sindex: EMPTY as u8,
    from_ptype: NO_PIECE as u8,
    to_ptype: NO_PIECE as u8,
    ep_sindex: NO_EP as u8,
    ep_cap_sindex: NO_EP as u8,
    promotion_ptype: NO_PIECE as u8,
};

pub const EMPTY_BMOVE_LIST: BmoveList = BmoveList {
    list: [EMPTY_BMOVE; BMOVE_LIST_LEN],
    index: 0,
};

pub struct BmoveList {
    pub list: [Bmove; BMOVE_LIST_LEN],
    pub index: usize,
}

pub struct Bmove {
    pub mtype: u8,
    pub from_sindex: u8,
    pub to_sindex: u8,
    pub from_ptype: u8,
    pub to_ptype: u8,
    pub ep_sindex: u8,
    pub ep_cap_sindex: u8,
    pub promotion_ptype: u8,
}

pub struct Square {
    pub stype: usize,
    pub ptype: usize,
    pub pindex: usize,
}

pub struct Board {
    pub sqrs: [Square; SQRS_LEN],
    pub plist: [usize; PLIST_LEN],
    pub plist_len: [usize; PLIST_LEN],
    pub stm: usize,
    pub non_stm: usize,
    pub castle: usize,
    pub ep_sqr: usize,
    pub ep_cap_sqr: usize,
    pub halfmove: usize,
    pub fullmove: usize,
    pub check_count: usize,
    pub pad_map: [usize; SQRS_LEN],
    pub ep_pin: usize,
    pub castle_history: [usize; HISTORY_LEN],
    pub ep_sqr_history: [usize; HISTORY_LEN],
    pub ep_cap_sqr_history: [usize; HISTORY_LEN],
    pub halfmove_history: [usize; HISTORY_LEN],
    pub history_index: usize,
    pub game_state: usize,
}

fn get_new_board() -> Board {
    let mut board: Board = Board {
        sqrs: [OFFBOARD_SQUARE; SQRS_LEN],
        plist: [EMPTY; PLIST_LEN],
        plist_len: [EMPTY; PLIST_LEN],
        stm: WHITE,
        non_stm: BLACK,
        castle: NO_CASTLE,
        ep_sqr: NO_EP,
        ep_cap_sqr: NO_EP,
        halfmove: 0,
        fullmove: 1,
        check_count: 0,
        pad_map: [PAD_UNSET; SQRS_LEN],
        ep_pin: PAD_UNSET,
        castle_history: [NO_CASTLE; HISTORY_LEN],
        ep_sqr_history: [EMPTY; HISTORY_LEN],
        ep_cap_sqr_history: [EMPTY; HISTORY_LEN],
        halfmove_history: [0; HISTORY_LEN],
        history_index: 0,
        game_state: GS_ONGOING,
    };

    for i in MAILBOX.iter() {
        board.sqrs[*i].stype = EMPTY;
    }

    board.plist_len[WHITE] = WHITE + 1;
    board.plist_len[BLACK] = BLACK + 1;

    board
}

pub fn get_startpos() -> Board {
    fen(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR",
        "w",
        "KQkq",
        "-",
        "0",
        "1",
    )
}

pub fn fen(
    sqrs: &str,
    stm: &str,
    castle: &str,
    en_passant: &str,
    halfmove: &str,
    fullmove: &str,
) -> Board {
    let mut board: Board = get_new_board();

    // fill squares
    let mut mailbox_index: usize = 0;
    for c in sqrs.chars() {
        if mailbox_index >= MAILBOX_LEN {
            return get_startpos();
        }

        if c == 'K' {
            add_piece(&mut board, &WHITE, &KING, &MAILBOX[mailbox_index]);
        } else if c == 'Q' {
            add_piece(&mut board, &WHITE, &QUEEN, &MAILBOX[mailbox_index]);
        } else if c == 'R' {
            add_piece(&mut board, &WHITE, &ROOK, &MAILBOX[mailbox_index]);
        } else if c == 'B' {
            add_piece(&mut board, &WHITE, &BISHOP, &MAILBOX[mailbox_index]);
        } else if c == 'N' {
            add_piece(&mut board, &WHITE, &KNIGHT, &MAILBOX[mailbox_index]);
        } else if c == 'P' {
            add_piece(&mut board, &WHITE, &PAWN, &MAILBOX[mailbox_index]);
        } else if c == 'k' {
            add_piece(&mut board, &BLACK, &KING, &MAILBOX[mailbox_index]);
        } else if c == 'q' {
            add_piece(&mut board, &BLACK, &QUEEN, &MAILBOX[mailbox_index]);
        } else if c == 'r' {
            add_piece(&mut board, &BLACK, &ROOK, &MAILBOX[mailbox_index]);
        } else if c == 'b' {
            add_piece(&mut board, &BLACK, &BISHOP, &MAILBOX[mailbox_index]);
        } else if c == 'n' {
            add_piece(&mut board, &BLACK, &KNIGHT, &MAILBOX[mailbox_index]);
        } else if c == 'p' {
            add_piece(&mut board, &BLACK, &PAWN, &MAILBOX[mailbox_index]);
        } else if ('1'..='8').contains(&c) {
            mailbox_index += c as usize - '1' as usize;
        }

        if c != '/' {
            mailbox_index += 1;
        }
    }
    if mailbox_index != MAILBOX_LEN {
        return get_startpos();
    }

    // set stm and non stm
    if stm == "w" {
        board.stm = WHITE;
        board.non_stm = BLACK;
    } else if stm == "b" {
        board.stm = BLACK;
        board.non_stm = WHITE;
    } else {
        return get_startpos();
    }

    // set castle rights
    if castle != "-" {
        for c in castle.chars() {
            if c == 'K' {
                board.castle |= CASTLE_WK;
            } else if c == 'Q' {
                board.castle |= CASTLE_WQ;
            } else if c == 'k' {
                board.castle |= CASTLE_BK;
            } else if c == 'q' {
                board.castle |= CASTLE_BQ;
            } else {
                return get_startpos();
            }
        }
    }

    // set en passant
    if en_passant != "-" {
        board.ep_sqr = EMPTY;
        let mut c = en_passant.chars();
        let file: char = c.next().unwrap_or(' ');
        let rank: char = c.next().unwrap_or(' ');

        if ('a'..='h').contains(&file) {
            board.ep_sqr += file as usize - 'a' as usize;
        }

        if ('1'..='8').contains(&rank) {
            board.ep_sqr += A8 + SQRS_WIDTH * ('8' as usize - rank as usize);
        }

        if board.stm == WHITE && (A6..=H6).contains(&board.ep_sqr) {
            board.ep_cap_sqr = (board.ep_sqr as isize + BPAWN_FORWARD) as usize;
        } else if board.stm == BLACK && (A3..=H3).contains(&board.ep_sqr) {
            board.ep_cap_sqr = (board.ep_sqr as isize + WPAWN_FORWARD) as usize;
        } else {
            return get_startpos();
        }
    }

    // halfmove
    match halfmove.parse::<usize>() {
        Ok(h) => board.halfmove = h,
        Err(_) => return get_startpos(),
    }

    // fullmove
    match fullmove.parse::<usize>() {
        Ok(f) => board.fullmove = f,
        Err(_) => return get_startpos(),
    }

    board
}

fn print_sqr(board: &Board, sindex: &usize) {
    if board.sqrs[*sindex].stype == EMPTY {
        print!(" - ");
    } else if board.sqrs[*sindex].stype == OFFBOARD {
        print!("...");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == KING {
        print!(" K ");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == QUEEN {
        print!(" Q ");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == ROOK {
        print!(" R ");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == BISHOP {
        print!(" B ");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == KNIGHT {
        print!(" N ");
    } else if board.sqrs[*sindex].stype == WHITE && board.sqrs[*sindex].ptype == PAWN {
        print!(" P ");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == KING {
        print!(" k'");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == QUEEN {
        print!(" q'");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == ROOK {
        print!(" r'");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == BISHOP {
        print!(" b'");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == KNIGHT {
        print!(" n'");
    } else if board.sqrs[*sindex].stype == BLACK && board.sqrs[*sindex].ptype == PAWN {
        print!(" p'");
    }
}

fn print_stm(board: &Board) {
    if board.stm == WHITE {
        print!("w");
    } else if board.stm == BLACK {
        print!("b");
    }
}

fn print_castle(board: &Board) {
    if board.castle == NO_CASTLE {
        print!("-");
    }
    if board.castle & CASTLE_WK == CASTLE_WK {
        print!("K");
    }
    if board.castle & CASTLE_WQ == CASTLE_WQ {
        print!("Q");
    }
    if board.castle & CASTLE_BK == CASTLE_BK {
        print!("k");
    }
    if board.castle & CASTLE_BQ == CASTLE_BQ {
        print!("q");
    }
}

pub fn print_board(board: &Board) {
    print!("    ");
    print_stm(board);
    print!(" ");
    print_castle(board);
    print!(
        " {} {} {}",
        SQUARE_NAMES[board.ep_sqr], board.halfmove, board.fullmove
    );
    println!();

    println!("  --------------------------");
    for rank in 0..MAILBOX_HEIGHT_WIDTH {
        print!("{} |", MAILBOX_HEIGHT_WIDTH - rank);
        for file in 0..MAILBOX_HEIGHT_WIDTH {
            let index: usize = MAILBOX[rank * MAILBOX_HEIGHT_WIDTH + file];
            print_sqr(board, &index);
        }
        println!("|");
    }
    println!("  --------------------------");
    println!("    a  b  c  d  e  f  g  h  \n");
}

pub fn print_pad_map(board: &Board) {
    for rank in 0..MAILBOX_HEIGHT_WIDTH {
        print!("{} |", MAILBOX_HEIGHT_WIDTH - rank);
        for file in 0..MAILBOX_HEIGHT_WIDTH {
            let index: usize = MAILBOX[rank * MAILBOX_HEIGHT_WIDTH + file];
            print!(" {:^3} ", board.pad_map[index]);
        }
        println!("|");
    }
}

pub fn add_piece(board: &mut Board, color: &usize, ptype: &usize, sindex: &usize) {
    if *ptype == KING {
        board.sqrs[*sindex].stype = *color;
        board.sqrs[*sindex].ptype = *ptype;
        board.sqrs[*sindex].pindex = *color;
        board.plist[*color] = *sindex;
    } else {
        board.sqrs[*sindex].stype = *color;
        board.sqrs[*sindex].ptype = *ptype;
        board.sqrs[*sindex].pindex = board.plist_len[*color];
        board.plist[board.plist_len[*color]] = *sindex;
        board.plist_len[*color] += 1;
    }
}

pub fn remove_piece(board: &mut Board, sindex: &usize) {
    let pindex: usize = board.sqrs[*sindex].pindex;

    board.plist_len[board.non_stm] -= 1;
    board.plist[pindex] = board.plist[board.plist_len[board.non_stm]];
    board.sqrs[board.plist[pindex]].pindex = pindex;

    board.sqrs[*sindex].stype = EMPTY;
    board.sqrs[*sindex].ptype = NO_PIECE;
    board.sqrs[*sindex].pindex = EMPTY;
}

pub fn get_bmove_string(bmove: &Bmove) -> String {
    let mut bmove_string: String = String::new();
    bmove_string.push_str(SQUARE_NAMES[bmove.from_sindex as usize]);
    bmove_string.push_str(SQUARE_NAMES[bmove.to_sindex as usize]);
    if bmove.mtype & BMOVE_PROMOTION == BMOVE_PROMOTION {
        if bmove.promotion_ptype as usize == KNIGHT {
            bmove_string.push('n');
        } else if bmove.promotion_ptype as usize == BISHOP {
            bmove_string.push('b');
        } else if bmove.promotion_ptype as usize == ROOK {
            bmove_string.push('r');
        } else if bmove.promotion_ptype as usize == QUEEN {
            bmove_string.push('q');
        }
    }

    bmove_string
}

pub fn make_str_move(board: &mut Board, str_move: &str) {
    let mut bmove_list: BmoveList = EMPTY_BMOVE_LIST;
    move_gen::move_gen(board, &mut bmove_list);

    for bmove in bmove_list.list[0..bmove_list.index].iter() {
        if str_move == get_bmove_string(bmove) {
            make_move::make_move(board, bmove);
        }
    }
}

pub fn perft(board: &mut Board, depth: usize) -> usize {
    if depth == 0 {
        return 1;
    }

    let mut nodes: usize = 0;
    let mut bmove_list: BmoveList = EMPTY_BMOVE_LIST;
    move_gen::move_gen(board, &mut bmove_list);

    if depth == 1 {
        return bmove_list.index;
    }

    for bmove in bmove_list.list[0..bmove_list.index].iter() {
        make_move::make_move(board, bmove);
        nodes += perft(board, depth - 1);
        make_move::unmake_move(board, bmove);
    }

    nodes
}

pub fn perft_start(board: &mut Board, depth: usize) {
    if depth == 0 {
        return;
    }

    let now: Instant = Instant::now();
    let mut nodes: usize = 0;
    let mut bmove_list: BmoveList = EMPTY_BMOVE_LIST;
    move_gen::move_gen(board, &mut bmove_list);
    for bmove in bmove_list.list[0..bmove_list.index].iter() {
        make_move::make_move(board, bmove);
        let move_nodes: usize = perft(board, depth - 1);
        nodes += move_nodes;
        println!("{}: {}", get_bmove_string(bmove), move_nodes);
        make_move::unmake_move(board, bmove);
    }
    let duration = now.elapsed().as_secs_f64();

    println!("\nTotal nodes: {} in {} seconds.", nodes, duration);
    println!("MNPS: {}\n", (nodes as f64 / 1000000.0) / duration);
}
